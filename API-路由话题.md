### 设置路由
- 设置初始位置
/initialpose

- 设置目标位置

/planning/mission_planning/goal

geometry_msgs/msg/PoseStamped

header:
  stamp:
    sec: 1725776080
    nanosec: 534254377
  frame_id: map
pose:
  position:
    x: 3751.197509765625
    y: 73738.8203125
    z: 0.0
  orientation:
    x: 0.0
    y: 0.0
    z: 0.22349487336419213
    w: 0.9747051049317038
    
#### 测试代码

/home/ubuntu/autoware_ws/src/test_pack/src/test_node.cpp

测试方法
 启动autoware默认模拟器
 -见ego启动文档
 启动测试节点（代码中的goal坐标需要和地图道路匹配，才能路由成功）
 ros2 run test_pack test_node

### 获取路由 
 起点和目标点

/api/routing/route

header:
  stamp:
    sec: 1725770437
    nanosec: 129188143
  frame_id: map
data:
- start:
    position:
      x: 3772.8564453125
      y: 73732.578125
      z: 19.308
    orientation:
      x: -0.0012512443694238427
      y: -0.0021653376321476596
      z: -0.5003241194088885
      w: 0.8658345807590426
  goal:
    position:
      x: 3711.156494140625
      y: 73698.578125
      z: 19.34877186523416
    orientation:
      x: 0.0
      y: 0.0
      z: 0.8622991743622388
      w: 0.5063991843340604
  segments:
  - preferred:
      id: 112
      type: lane
    alternatives: []
  - preferred:
      id: 19
      type: lane
    alternatives: []
  - preferred:
      id: 9232
      type: lane
    alternatives: []
  - preferred:
      id: 11
      type: lane
    alternatives: []
  - preferred:
      id: 109
      type: lane
    alternatives: []
---

