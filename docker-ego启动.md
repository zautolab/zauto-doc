### 启动docker
- 启动
默认不保存镜像，以使xhost生效，可以输出gui

cd /home/dev/autoware

./docker/run.sh --devel

启动名为zauto_x的容器

进入容器
docker exec -it   zauto_x /bin/bash

- 性能配置
设置到.bashrc或集成到run.sh

if [ ! -e /tmp/multicast_is_set ]; then
sudo ip link set lo multicast on
touch /tmp/multicast_is_set
fi
export CYCLONEDDS_URI=/autoware_data/cyclonedds_config.xml

if [ ! -e /tmp/kernel_network_conf_is_set ]; then
sudo sysctl -w net.core.rmem_max=2147483647
sudo sysctl -w net.ipv4.ipfrag_time=3
sudo sysctl -w net.ipv4.ipfrag_high_thresh=134217728 # (128 MB)
fi
export ROS_LOCALHOST_ONLY=1

export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp

- cp ego配置
cp -r /home/ubuntu/autoware_ws/autoware/param/ego_sensor_kit /autoware/install/individual_params/share/individual_params/config/default/ego_sensor_kit

宿主机拷贝
docker cp  /home/ubuntu/autoware_ws/autoware/param/ego_sensor_kit zauto_x:/autoware/install/individual_params/share/individual_params/config/default/ego_sensor_kit

docker cp  /home/ubuntu/autoware_ws/src/zauto_vehicle_interface/lib/libcontrolcan.so zauto_x:/usr/lib/libcontrolcan.so

## =========集成的启动操作==============



cd /home/ubuntu/autoware_ws/bin

- 启动感知设备

./run.sh

- 启动自动驾驶(集成autoware)

./run_autoware.sh

（mqtt订阅需要单独启动）

## =========非集成的启动操作==============

- 导入path
```
source /autoware/install/local_setup.bash

source /home/ubuntu/autoware_ws/install/local_setup.bash

source /home/ubuntu/autoware_ws/src/mqtt_adapter/install/setup.bash
```
## 启动autoware模拟器
ros2 launch autoware_launch planning_simulator.launch.xml map_path:=/autoware_map/sample-map-planning vehicle_model:=sample_vehicle sensor_model:=sample_sensor_kit

# zauto模拟
ros2 launch autoware_launch planning_simulator.launch.xml map_path:=/autoware_map/zauto_411 vehicle_model:=zauto_vehicle sensor_model:=ego_sensor_kit

## 启动autoware
ros2 launch autoware_launch autoware.launch.xml data_path:=/autoware_data map_path:=/autoware_map/sample-map-planning vehicle_model:=sample_vehicle sensor_model:=sample_sensor_kit

ros2 launch autoware_launch autoware.launch.xml data_path:=/autoware_data map_path:=/autoware_map/sample-map-planning vehicle_model:=zauto_vehicle sensor_model:=zauto_sensor_kit

ros2 launch autoware_launch autoware.launch.xml data_path:=/autoware_data map_path:=/autoware_map/zauto_out vehicle_model:=zauto_vehicle sensor_model:=ego_sensor_kit


## 单独测试某项

ros2 launch zauto_sensor_lslidar lsliar_c32.launch.xml vehicle_model:=zauto_vehicle vehicle_mirror_param_file:=install/zauto_vehicle_description/config/mirror.param.xml

- 镭神雷达启动(也在autoware_ws目录)

ros2 launch lslidar_driver lslidar_c32_launch.py

启动角雷达（单线,暂不启动）

ros2 launch lslidar_m10p_driver lsm10p_uart_launch.py

- 启动imu

ros2 launch fdilink_imu ahrs_driver.launch.py

- 启动车机

ros2 launch zauto_vehicle_interface vehicle_ctrl_launch.xml

- 启动mqqt 适配器

ros2 run mqtt_rclpy mqtt_ps


