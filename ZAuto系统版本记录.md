## 版本和时间主线

|版本代号|版本号|阶段|
|:----:|:----:|:----:|
|zauto_summer|summer_c(zauto_autoware_c)|2023.7-|
|zauto_b|zauto_b|2024.1-|



### zauto_summer (代号summer)

基于autoware 2023.9版本

- 启动 2023.7

使用zauto套件方案v1

- 阶段1 2023.7-2023.12

基础研究，环境搭建测试，设备集成

summer_a summer_b 已废弃

summer_c  (系统内名称为autoware_zauto_c) 

2023.11 

完成Slam低速定位和导航测试

summer_c为最后版本

- 阶段2 2024.1-2024.6

优化summer_c

### zauto_b

- 启动 2024.1

基于autoware2023.12版本

计划迭代为autoware v1.0版本

尚未独立测试

- zauto_c

 20240527-devel-cuda

docker pull ghcr.io/autowarefoundation/autoware-openadk:20240528-devel-cuda-amd64
