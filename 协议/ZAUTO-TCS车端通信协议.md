---
ZAUTO 车端通信协议 DCU-TCS Beta
- 0.91 init
- 0.92 add topic {deviceid}
zauto-lab 
---

### 通用说明

车端和mqtt服务器通信，topic带后缀/up /down的标识上报和下发
消息字段必须包括ts deviceid body

topic统一使用/devices/{deviceid}/开头

- 示意
```
/devices/{deviceid}/login/up
{
     "ts": "20221206114512000", 
    "deviceid": "zauto_01", 
    "body": {
        
       
    }
}

ts毫秒时间戳
deviceid 设备的obuid 在当前业务项目中唯一
```

### 数据流向

TCS-MQTT------ DCU

 | <---login/up--------  |  终端启动，上线

 | -----login/down------> |  TCS对终端的上线响应

 | <----logout/up---------|  终端下线，非必须，超时会判定为离线

 | <----report--------------|  DCU周期上报车辆信息，如位置、速度、CAN数据、剩余电量、驾驶员ID等，周期消息，建议频率10HZ


 | <---subscribe-------- |  DCU注册并订阅消息，可不发，用于设定是否订阅TCS通知和频率。默认notify 1hz, warning planning command-s2v 实时多次下发

 | ----notify----------> |  MQTT周期下发网络信息，如推荐车速、路侧传感器发现的周边障碍物，周期消息，建议频率10HZ

 | ----warning---------> |  MQTT下发预警信息，如“前方车辆，请注意避让”，此消息对自动驾驶也有意义

 | ----planning--------> |  MQTT下发路径规划，含两种场景：MQTT路径规划变化主动通知DCU；DCU发现notify下发的route时间戳和本地不一致则用planning查询路径规划


 | <---devlog---------- |  DCU上报设备关键日志，如“XX传感器初始化成功”，请仅上报关键日志，控制流量

 | <---planning-find---- |  DCU查询现有路径规划

 | <---planning-refuse--|  可选消息：DCU拒绝TCS下发的路径规划（如：DCU路径不可达、剩余电量不足），拒绝后TCS置该DCU为任务失败（避免反复重发、反复拒绝），重试需重建任务

 | <---planning-arrive-- |  可选消息：DCU通知TCS达到planning指定的目的地，到达后TCS会清除路径规划。不发此消息则TCS根据DCU的位置自动判断是否到达

 | ----------platoon---------> |  如果该车是编队方式行驶，周期下发编队信息

 | ----command-s2v------> |  下发“云->车(openapi→TCS→MQTT→dcu)”命令请求

 | <---command-s2v-resp--|  上报“云->车(openapi→TCS→MQTT→dcu)”命令应答

 | <---command-v2s-------- |  上报“车->云(dcu→MQTT→TCS→openapi)”命令请求

 | ---command-v2s-resp-> |  下发“车->云(dcu→MQTT→TCS→openapi)”命令应答

## 1.设备上线和下线
### 上线

- /devices/{deviceid}/login/up

```

{
     "ts": "20231206114512000", 
    "deviceid": "zauto_01", 
    "body": {
        "version":"1.0",
        "ip": "1.1.1.1",
        "mac": "00:00:00:00:00:00",
        "device_type": "zauto_1.0",
       
    }
}
```
### 上线应答

- topic

 /devices/{deviceid}/login/down

```

{
    "ts": "20221206114512000", 
    "deviceid": "zauto_01", 
    "body": {
        "code": "200",
        "mac": "D4:12:43:33:B1:C4",
        "origin_lat":22.123,
        "origin_lon":133.123,
        "type": "CAR",
        "sub_type": "TRANS_CAR",
        "model_id":"郑航1号",
        "length":4.9,
        "width":1.76,
        "height":1.94,
        "rtk_url":"1.1.1.1",
        "rtk_port":8002,
        "rtk_mount":"",
        "rtk_user":"rtk001",
        "rtk_password":"password",
        "offset_x":0,
        "offset_y":0.8,
        "uwb_x":1234,
        "uwb_y":316,
        "uwb_heading":64.4,
        "priority":2.0,
        "support_auto_drive":true,
        "sys_id":"airport",
        "sys_name":"智能交通系统",
        "sys_language":"zh_CN",
        "road_direction":"R"
    }
}
```
### 设备下线


- topic

 /devices/{deviceid}/logout/up

```
{
    "ts": "20221206114512000", 
    "deviceid": "zauto_01", 
    "body": {
        "reason": "close"
     
    }
 }
 ```
 
### 下线应答
一般终端可选处理

仅支持有连接对象（obu_proxy或直连）

- topic

 /devices/{deviceid}/logout/down

```
{
    "ts": "20221206114512000", 
    "deviceid": "zauto_01", 
    "body": {
        "code": "200"
    }
}
```
code 200 正常

## 2.上报状态

- 周期 10hz

- 数据流向

  DCU-->MQTT 周期上报车辆信息，如位置、速度、CAN数据、剩余电量、驾驶员ID等，周期消息，建议频率10HZ

- topic

/devices/{deviceid}/report

- 说明

  DCU->MQTT: DCU定期为DCU上报信息（建议频率10HZ）
  
  所有字段均可选填，不填则沿用上一消息的值（如上报档位为D，则后续消息忽略gear字段时，档位仍为D）

```
{
    "ts": "20221206114512000", 
    "deviceid": "zauto_01", 
    "body": {
            "id":"zauto_01",
            "ts":"20210325142909123",
            "gps_time":"124671288",
            "status":"A",
            "pos_type":"RTK2",
            "lat":25.2123511,
            "lon":132.1315321,
            "ele":15.2,
            "heading":25.3,
            "speed":21.5,
            "accel":2.5,
            "pos_quality":"RTK_FIX",
            "hdop":0.3,
            "sats_num":12,
            "brake":20.5,
            "steer":5.5,
            "gear":"D",
            "odom":100,// 里程计，通过两次差值=本次行进距离
            "door":"L1,L2,B1",
            "light":"LR",
            "epb":"OFF",
            "power":65,
            "vevent":0,
           "driver_id":"小高",
           "device_type":"tcs-d-adp-1.0",
            "target_lat":25.2123511,
            "target_lon":132.1315321,
          //障碍信息 备用  
        "obstacles": [            //周边障碍物（本车传感器发现的障碍物）
            {
                "obs_id":"obs-1",        //传感器给障碍物分配的ID，如果希望连续跟踪某障碍物，请为它连续编为同一个obs_id
                "ts":"20210325142909123",//发现障碍物的时间戳，不填则为当前时间，正确填此字段有利于障碍物合并和轨迹推测
                "type":"OBJECT",         //类型(子类型)，包括枚举值：CAR(CAR,POLICE_CAR,RESCUE_CAR,PATROL_CAR)/BUS(BUS,AMBULANCE,SCHOOL_BUS,SHUTTLE_BUS,MINI_BUS)/
                "sub_type":"STONE",      //TRUCK(TRUCK,PUMPER,DANGEROUS_TRUCK,LUGGAGE_TRACTOR,PLANE_TRACTOR,MINE_CAR,STAIR_CAR)/MOTOR(MOTOR,MOTOR_TRICYCLE)/
                                         //AGV(AGV,ROBOT)/BICYCLE(BICYCLE,TRICYCLE,E_BICYCLE,E_TRICYCLE,BABY_CAR)/UP_DEVICE(UP_DEVICE)/PEDESTRIAN(PEDESTRIAN)/ANIMAL(ANIMAL)/
                                         //PLANE(PLANE,UAV)/TRAIN(TRAIN,METRO,LIGHT_RAIL)/SHIP(SHIP)/OBJECT(OBJECT,CONE,STONE,ROAD_RELIC,PUDDLE,RSU) //实际子类型以DB表t_om_MQTT_type为准
                                         //传感器无法识别障碍物类型则忽略这两个字段或填：type=OBJECT, sub_type=空
                "ttl":5,                 //多长时间不再上报此障碍物则删除障碍物，秒，不填则为3秒
                "lat":32.2312311,        //经纬度，必填
                "lon":125.5321231,       //经纬度，必填
                "ele":10.0,              //高度，单位：米，不填则为0米
                "heading":15.3,          //所处位置的头指向与正北方向顺时针角度，取值0-360，不填则为-1(表示无法识别头指向)
                "speed":0,               //速度，单位km/h，不填则为0km/h
                "width":1.1,             //宽度，单位：米，不填则为0m(表示无法识别尺寸)
                "height":0.5,            //高度，单位：米，不填则为0m(表示无法识别尺寸)
                "length":2.2             //长度，单位：米，不填则为0m(表示无法识别尺寸)
            },
            {
                "obs_id":"obs-2",
                "type":"CAR",
                "sub_type":"CAR",
                "ttl":2,
                "lat":33.2312311,
                "lon":133.5321231,
                "heading":11.2,
                "speed":15.5,
                "length":6.2,
                "width":3.5,
                "height":2.5
            }
        ],
        "target_obstacles": [ //如果填写了target_lat/target_lon，返回这附近50米的障碍物
            {
                "obs_id":"obs-3",
                "type":"CAR",
                "sub_type":"CAR",
                "ttl":2,
                "lat":33.2312311,
                "lon":133.5321231,
                "heading":11.2,
                "speed":15.5,
                "length":6.2,
                "width":3.5,
                "height":2.5
            }
        ]
 } }
 ```

## 3.订阅

- 说明
--------------------------------------------------------------------------------------------------
  DCU->MQTT: DCU订阅业务消息（可选消息，DCU有需求时发送一次）
  不发此消息则用默认值，默认值如下示例
  仅可订阅“MQTT发给DCU”的消息，DCU上报MQTT的有消息由DCU自身控制上报频率，
  其中notify为周期消息，取值为上报间隔(ms)，其他消息取值为0/1

--------------------------------------------------------------------------------------------------
- topic

/devices/{deviceid}/subscribe
```
{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01", 
    "body": {
        "version":"V1.0",        //DCU版本号，仅用于记录日志
        "device_type": "ZATUO_V1",  //装置类型，仅用于记录日志
        "subscribe":{            //订阅消息，取值为间隔多少毫秒发一次，非周期消息填1，不需要的消息不填或填0，仅含MQTT发给DCU的消息
             "notify"  : 1000,   //MQTT->DCU：周期信息下发，取值范围0或100-10000，0=不发
             "warning" : 0,      //MQTT->DCU：预警信息下发，取值范围0/1，0=不发
             "planning": 0,      //MQTT->DCU：路径规划下发请求，取值范围0/1，0=不发
             "platoon" : 0,      //MQTT->DCU：编队信息下发，取值范围0或100-10000，0=不发 /备用
             "command-s2v" : 0,  //MQTT->DCU：网络命令下发，取值范围0/1，0=不发
             "command-v2s" : 0,  //MQTT->DCU：下发车辆请求命令的应答，取值范围0/1，0=不发
        }
    }
}
```
## 4.通知

- 说明
--------------------------------------------------------------------------------------------------
  MQTT->DCU: MQTT定期为DCU下发信息（默认1HZ，可通过“subscribe”消息修改频率）
 
--------------------------------------------------------------------------------------------------
- topic

/devices/{deviceid}/notify
```
{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "MQTT_id":"粤B 53253",       //MQTT为本车分配的ID
        "ts":"20210304123456001",   //MQTT时间戳（MQTT当前时刻，为空表示未连接到云，无后续字段），UTC时间，格式为YYYYMMDDHHmmSSsss
        "lim_speed":30.0,           //道路限速：单位km/h（综合了地图限速、限速牌限速、雨雾等交通事件限速）
        "sug_speed":30.0,           //推荐车速：通常等于lim_speed，如拥堵时则可能推荐一个小于lim_speed的速度
        "lat":25.2123511,           //车辆纬度
        "lon":132.1315321,          //车辆经度
        "heading":15.3,             //车辆头指向（正北为0°，顺时一周360°）
        "ele":15.2,                 //车辆海拔，单位：米
        "pos_quality":"RTK_FIX",    //定位精度，NONE/GNSS/DGNSS/PPS/RTK_FIX/RTK_FLOAT/INS/MANUAL/SIMULATION/UWB
        "tag":"AIRPORT",            //透传参数
        "route":"20210325142909123",//路径规划时间戳，检查是否一致 不一致需要请求新的plan
        "destination":"航站2号",//路径规划的目的地
        "obstacles": [              //周边障碍物（融合了路侧传感器和车辆上报的障碍物，含自身上报的障碍物），编队行驶时含前后兄弟车辆（用obs_id对应编队车辆的MQTT_id）
            {
                "obs_id":"obs_1",        //传感器给障碍物分配的ID，同一个obs_id tcs可进行跟踪
                "ts":"20210325142909123",//发现障碍物的时间戳，不填则为当前时间，正确填此字段有利于障碍物合并和轨迹推测
                "type":"OBJECT",         //类型(子类型)，包括枚举值：CAR(CAR,POLICE_CAR,RESCUE_CAR,PATROL_CAR)/BUS(BUS,AMBULANCE,SCHOOL_BUS,SHUTTLE_BUS,MINI_BUS)/
                "sub_type":"STONE",      //TRUCK(TRUCK,PUMPER,DANGEROUS_TRUCK,LUGGAGE_TRACTOR,PLANE_TRACTOR,MINE_CAR,STAIR_CAR)/MOTOR(MOTOR,MOTOR_TRICYCLE)/
                                         //AGV(AGV,ROBOT)/BICYCLE(BICYCLE,TRICYCLE,E_BICYCLE,E_TRICYCLE,BABY_CAR)/UP_DEVICE(UP_DEVICE)/PEDESTRIAN(PEDESTRIAN)/ANIMAL(ANIMAL)/
                                         //PLANE(PLANE,UAV)/TRAIN(TRAIN,METRO,LIGHT_RAIL)/SHIP(SHIP)/OBJECT(OBJECT,CONE,STONE,ROAD_RELIC,PUDDLE,RSU) //实际子类型以DB表t_om_MQTT_type为准
                                         //传感器无法识别障碍物类型则忽略这两个字段或填：type=OBJECT, sub_type=空
                "ttl":5,                 //多长时间不再上报此障碍物则删除障碍物，秒，不填则为3秒
                "lat":32.2312311,        //经纬度，必填
                "lon":125.5321231,       //经纬度，必填
                "ele":10.0,              //高度，单位：米，不填则为0米
                "heading":15.3,          //所处位置的头指向与正北方向顺时针角度，取值0-360，不填则为-1(表示无法识别头指向)
                "speed":0,               //速度，单位km/h，不填则为0km/h
                "width":1.1,             //宽度，单位：米，不填则为0m(表示无法识别尺寸)
                "height":0.5,            //高度，单位：米，不填则为0m(表示无法识别尺寸)
                "length":2.2             //长度，单位：米，不填则为0m(表示无法识别尺寸)
            },
            {
                "obs_id":"obs-2",
                "type":"CAR",
                "sub_type":"CAR",
                "ttl":2,
                "lat":33.2312311,
                "lon":133.5321231,
                "ele":10.0,
                "heading":11.2,
                "speed":15.5,
                "length":6.2,
                "width":3.5,
                "height":2.5
            }
        ],
        "lights": [
            {                           //周边红绿灯
                "light_id":"light_NORTH1", //红绿灯ID，从地图中读取
                "light_name":"1号信号灯", //红绿灯名称，从地图中读取
                "direction":"STRAIGHT",   //红绿灯控制的方向，枚举值：ALL/STRAIGHT/LEFT/RIGHT/TURN
                "lat":31.2231351,         //红绿灯安装位置，纬度
                "lon":132.2312351,        //红绿灯安装位置，经度
                "heading":25.3,           //红绿灯安装头指向（正北为0°，顺时针循环一周为360°），因为朝向车辆，所以通常和车辆头指向成180°夹角
                "tag":"AIRPORT",          //红绿灯附加信息，多个用“,”间隔
                "color":"RED",            //当前灯色，枚举值：NONE/GREEN/YELLOW/RED
                "remain_time":17,         //红绿灯倒计时，单位：秒，不支持倒计时则填-1
                "distance": 25.2          //如果本车途径此红绿灯，则填写到该灯停止线的距离（米），不途径的无此字段
                                          //TCS根据车辆所在车道预测途径的红绿灯，请DCU二次计算红绿灯
            },
            {
                "light_id":"intersection2.NORTH",
                "light_id":"2号信号灯",
                "direction":"LEFT",
                "lat":31.2231351,
                "lon":132.2312351,
                "heading":25.3,
                "color":"RED",
                "remain_time":17
           }
        ]
    }
}
```
## 5.预警

- 说明

--------------------------------------------------------------------------------------------------
 MQTT->DCU: MQTT下发预警信息，如“前方车辆，请注意避让”（默认不发，可通过“subscribe”订阅此消息）

--------------------------------------------------------------------------------------------------

//核心参数为type/level/params，目前主要支持的预警（type为UTF8简体中文，district依据TCS语种而定）:

//type="PLANE:MAJOR", params="distance=20, direction=FRONT" //到飞机的距离，粗略方位，枚举值: FRONT/BACK/LEFT/RIGHT/LEFT_FRONT/RIGHT_FRONT/LEFT_BACK/RIGHT_BACK

//type="PLANE:MINOR", params="distance=100, direction=FRONT" //同上

//type="PLANE:SUGGEST", params="distance=200, direction=FRONT" //同上

//type="BASE:FORBIDDEN_DISTRICT", params="district=1号跑道" //禁行区域名

//type="BASE:SPECIAL_FORBIDDEN_DISTRICT", params="sub_type=PLANE_TRACTOR, district=T3下穿隧道" //自身车型，禁行区域名

//type="BASE:RUN_RED_LIGHT", params="" //无参数

//type="CITS:I_WRONG_DIRECTION_WARNING", params="" //无参数


//场景举例：协作式借道避障场景，

//  仅对双向单车道，TCS发现某车A前方有故障车F，则TCS通知A车停车(COOP_AVOID:STOP)或借逆行道避障(COOP_AVOID:AVOID)，如果借道时发现逆行道上有对向行驶的B车，则给B车发预警(COOP_AVOID:STOP)，

//  以上三个场景均通过para传递参数，参数内容为3辆车的位置，其中B车可能不存在。该预警在上述格局满足时每10秒发送一次，格局打破(如：A车借道完成/A车已经停车)后不再发送。

//  A和B均可能为自动驾驶车辆，根据预警产生借道避障(AVOID)或停车(STOP)动作。但TCS不会通知A车和B车“借道完成”和“再起步”，DCU自行决策。

A停车

//type="COOP_AVOID:STOP"， params="fault_car=CAR_F, fault_lat=22.1234567, fault_lon=133.1234567, cars=CAR_B, lat_1=22.1234567, lon_1=133.1234567"

A借道,B停车

-A借道

//type="COOP_AVOID:AVOID"， params="fault_car=CAR_F, fault_lat=22.1234567, fault_lon=133.1234567, cars=CAR_B, lat_1=22.1234567, lon_1=133.1234567"

-B

//type="COOP_AVOID:STOP"， params="cars=CAR_A, lat_1=22.1234567, lon_1=133.1234567, fault_car=CAR_F, fault_lat=22.1234567, fault_lon=133.1234567"

- topic

/devices/{deviceid}/warning
```

{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "ts":"20230325142909123",        //预警发生时间戳（不一定是当前时刻），UTC时间，格式为YYYYMMDDHHmmSSsss
        "type":"COOP_AVOID:A_AVOID",     //预警类型名
        "name":"I Cooperation Avoid",    //预警名称
        "level":"MINOR",                 //预警级别，枚举值：SUGGEST/MINOR/MAJOR/FATAL(严重程度递增)
        "params":"type="COOP_AVOID:AVOID"， params="fault_car=CAR_F, fault_lat=22.1234567, fault_lon=133.1234567, cars=CAR_B, lat_1=22.1234567, lon_1=132.1234567"", //预警参数
        "driver_id":"张三12345",         //预警时本车司机ID
        "content":"前方200米有航空器",   //预警正文
        "sound":"beep",                  //预警播放声音，为空则用content，"beep"表示发预警音
        "lat":22.1234516,                //预警时本车位置
        "lon":132.1234156                //预警时本车位置
    }
}
```

## 6.日志上报

- 说明

--------------------------------------------------------------------------------------------------

// DCU->MQTT: DCU上报设备关键日志，如“初始化成功”，请仅上报关键日志，控制流量，至少记录：

// 1、DCU成功对接到MQTT

// 2、DCU与MQTT正常断连

// 3、DCU版本变化

// 4、DCU发生影响功能的故障

// 5、DCU启动自动驾驶/远程驾驶/编队等

// 6、DCU退出自动驾驶/远程驾驶/编队等

--------------------------------------------------------------------------------------------------
- topic

/devices/{deviceid}/devlog/up

{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "ts":"20210325142909123",   //日志发生时间戳（不一定是当前时刻或header.timestamp），UTC时间，格式为YYYYMMDDHHmmSSsss
        "type":"DCU_UPDATE_OK",     //日志类型，自拟，方便console分类统计
        "content":"DCU升级到V1.2版" //日志文本
    }
}

## 7.规划查询

- 说明
--------------------------------------------------------------------------------------------------

 DCU->MQTT: DCU请求路径查询现有路径规划

--------------------------------------------------------------------------------------------------
- topic

/devices/{deviceid}/planning-find
```
{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "ts":"20210325142909123" //DCU本地的路径规划时间戳（不一定是当前时刻），允许不填
    }
}


```

## 8.规划

- 说明
--------------------------------------------------------------------------------------------------

 MQTT->DCU: MQTT下发路径规划（默认发，可通过“subscribe”订阅此消息），应用场景含：

 1、TCS云服务产生路径规划时，实时下发给DCU

 2、DCU登录时通过“notify”消息的route字段得知MQTT已有路径规划

 3、DCU运行时通过“notify”消息的route字段得知MQTT路径规划时间戳和DCU不一致，请求下发最新的路径规划

MQTT下发的路径规划为连续的轨迹点（点和点的间距不确定，通常为1-100米），精度到车道级，但DCU可以选择该车道的同向兄弟车道
行进，最终达到目的地即可（MQTT不会下发发车、停车灯指令，请DCU自行按路径规划运行到目的地）

--------------------------------------------------------------------------------------------------
- topic

/devices/{deviceid}/planning

- 成功规划
```

{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "ts":"20210325142909123",       //路径规划时间戳
        "area_id":"AREA_A",           //路径规划所在区域（如果该项目含多区域的话）
        "source":"CONSOLE",             //哪个层次触发的路径规划：VUI=(司机)在手机/车载屏创建，DCU=自动驾驶单元，MQTT=MQTT创建，TCS=中心云创建，CONSOLE=北向创建
        "reason":"自动驾驶请求路径规划",//创建路径规划原因
        "destination":"航站2号门",    //目的地名，其位置为key_points中type=ROUTE_STOP的点
        "params":"",                    //可选字段，可自拟，此路径规划的参数
        "key_points":  [                //路径规划的关键点，通常至少包含起点ROUTE_START、终点ROUTE_STOP
            {
                "type":"ROUTE_START",   //关键点类型，如：起点ROUTE_START、终点ROUTE_STOP、途经点PASSING、站点STATION，可忽略
                "lat":32.1315312,       //关键点位置
                "lon":133.2123511,      //关键点位置
                "heading":343           //途径此点要求的头指向（正北为0°，顺时针循环一周为360°），终点必填此字段，如终点为车站，则必须以合适的头指向进站
            },
            {
                "lat":32.13153152,
                "lon":133.6345314
            },
            {
                "type":"STATION",
                "lat":32.1315512,
                "lon":133.6345341
            },
            {
                "type":"ROUTE_STOP",
                "lat":32.1315321,
                "lon":133.2123511,
                "heading":343,
                "name":"航院西门",          //可选字段，地址名称
                "tag":"PARKING,SIDE,CHARGE", //可选字段，地址描述，PARKING=停车位,SIDE=侧方停车,CHARGE=含充电桩
                "time":"20210325142909123"     //可选字段，规定到达此点的时间，不填则为不限到达时间
            }
        ],
        "trail":  [                     //路径规划结果（即：轨迹）
            {
                "lat":32.1315312,       //轨迹点位置
                "lon":133.2123511       //轨迹点位置
            },
            {
                "lat":32.1315352,
                "lon":133.6341534
            },
            {
                "lat":32.1315352,
                "lon":133.6345134
            },
            {
                "lat":32.1315312,       //车辆必须以最后两个轨迹点的连线构成的头指向进入终点，
                "lon":133.2123511       //以保证车辆以正确的方向进入车位
            }
        ]
    }
}
```
- 规划失败

场景3：路径规划失败，FORBID=TCS禁止路径规划, BAD_PARA=规划参数不正确, NOT_EXIST=没有直达路径, ERROR=其他错误
```
{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "ts":"",               //路径规划时间戳为空
        "last_ret":"NOT_EXIST" //错误原因：没有直达路径
    }
}
```

### 车端规划应答

- 说明
--------------------------------------------------------------------------------------------------

// DCU->MQTT: 可选消息：DCU拒绝TCS下发的路径规划（如：DCU路径不可达、剩余电量不足），拒绝后TCS置该DCU为任务失败（避免反复重发、反复拒绝），重试需重建任务

// TCS收到此消息后会下发一条"planning"消息，内容为清空现有路径规划

--------------------------------------------------------------------------------------------------
- topic

/devices/{deviceid}/planning-refuse

```

{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "ts":"20210325142909123", //路径规划的时间戳（注意不是当前时间，而是TCS下发的planning消息中的ts）
        "reason":"剩余电量不足"   //可选字段，拒绝原因（仅用于TCS记日志）
    }
}
```
 - 到达
 --------------------------------------------------------------------------------------------------

// DCU->MQTT: 可选消息：DCU通知TCS达到planning指定的目的地，到达后TCS会清除路径规划。不发此消息则TCS根据DCU的位置自动判断是否到达

// TCS收到此消息后会下发一条"planning"消息，内容为清空现有路径规划

--------------------------------------------------------------------------------------------------
- topic

/devices/{deviceid}/planning-arrive

```
{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "ts":"20210325142909123" //路径规划的时间戳（注意不是当前时间，而是TCS下发的planning消息中的ts）
    }
}
```
## 9.编队

- 说明

--------------------------------------------------------------------------------------------------

MQTT->DCU: 仅在本车处于编队中，MQTT定期为DCU下发编队信息（默认不下发，可通过“subscribe”本消息并推荐频率1HZ）

--------------------------------------------------------------------------------------------------

{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "MQTT_id":"渡C 00003",             //本车ID
        "platoon_id":"uuid123456",        //编队ID
        "platoon_name":"编队1",           //编队名称
        "create_time":"20210304123000000",//编队创建时间
        "end_addr":"2号站台",             //编队目的地，可选参数，也允许没有目的地，且即使有目的地，也不会产生路径规划
        "max_platoon_time":600,           //编队持续时长，单位秒，可选参数，超过时长TCS会解散编队或延长时长
        "MQTT_list":  [                    //编队车辆列表，注意是有顺序的，第一条为头车，最后一条为尾车
            {
                "MQTT_id":"渡C 00001",     //此车ID
                "ts":"20210325142909123", //此车上报位置的时间戳，可能晚于当前时间数百毫秒!!
                "status":"ADN",           //包含'C'=连接TCS正常/包含'E'=MQTT故障/包含'A'=自动驾驶中/包含'R'=远程驾驶中/包含'T'=任务中/包含'P'= 编队中/包含'F'=加油或充电中/包含'B'=繁忙中(不被任务调度)/包含'W'=允许预警/包含'N'=允许导航
                "lat":25.2123511,         //车辆纬度，可能为数百毫秒前的位置!!
                "lon":132.1315321,        //车辆经度，可能为数百毫秒前的位置!!
                "ele":15.2,               //车辆海拔，单位：米，可能为数百毫秒前的位置!!
                "heading":25.3,           //车辆头指向，正北为0°，顺时针旋转一周为360°
                "speed":21.5,             //车速，km/h
                "power":65,               //油量或电量，0-100％
                "vevent":0,               //车辆事件码，位域字段，末13位“CBA9876543210”分别代表“气囊弹出/故障/爆胎/雨刷状态改变/车灯改变/急刹/保留/装载危险品/车身稳定器启动/牵引力异常/ABS启动/越线停车/双闪灯亮”
                "tag":"wiper fault"       //附加信息，可选字段，该车用report上报tag信息，则这里传给编队的每一辆车
            }，
            {
                "MQTT_id":"渡C 00003",     //对接body.MQTT_id说明是本车，此例中，本车位3车编队中的第2辆
                "ts":"20210325142909123"
                "status":"ADN",
                "lat":25.2123511,
                "lon":132.1315321,
                "ele":15.2,
                "heading":25.3,
                "speed":21.5,
                "power":65,
                "vevent":0
            }，
            {
                "MQTT_id":"渡C 00007",
                "ts":"20210325142909123"
                "status":"ADN",
                "lat":25.2123511,
                "lon":132.1315321,
                "ele":15.2,
                "heading":25.3,
                "speed":21.5,
                "power":65,
                "vevent":0
            }
        ]
    }
}


## 10.S2V 云指令

- 说明

--------------------------------------------------------------------------------------------------

 MQTT->DCU: 下发“云->车(openapi→TCS→MQTT→dcu)”命令请求

 执行后发出应答消息command-s2v

--------------------------------------------------------------------------------------------------
- topic

/devices/{deviceid}/command-s2v

```

{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "ts":"20210304123456001",            //命令时间戳，“ts|command”构成唯一索引，即：每车不能在1ms内下发多条同一命令
        "command":"START",        //命令名，允许自定义命令名，必选字段
        "params":"force=true, direction=FRONT", //命令参数，允许自定义命参数，可选字段
        "description":"启动自动驾驶"         //给司机看的命令描述，可选字段
    }
}
```

- 指令应答
 --------------------------------------------------------------------------------------------------

 DCU->MQTT: 上报“云->车(openapi→TCS→MQTT→dcu)”命令应答

 用于应答网络下发的command-s2v消息
 
 --------------------------------------------------------------------------------------------------
- topic

/devices/{deviceid}/command-s2v-resp

```

{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "ts":"20210304123456001",     //命令时间戳，“ts|command”构成唯一索引，即：每车不能在1ms内下发多条同一命令
        "command":"START_AUTO_DRIVE", //命令名，允许自定义命令名，必选字段
        "result": "",                 //可选字段，自定义返回码，可在portal显示，或用于工作流的分支参数，
                                      //禁止和status枚举值(OK/ERROR/SUCCEED/FAILURE/TIMEOUT/CANCELED)重名!!!!

        //以下status选其一：

        //上报RUNNING表示命令没有执行完，后续还会回应答此消息
        "status": "RUNNING"

        //执行成功后应答SUCCEED
        "status": "SUCCEED"

        //失败，包括以下3种错误码
        "status": "FAILURE","error_msg": "001. parameter error"
        "status": "TIMEOUT","error_msg": "020. command timeouted"
        "status": "CANCELED","error_msg": "030. command canceled"
    }
}

```
## 11.V2S 云指令
- 说明
--------------------------------------------------------------------------------------------------

DCU->MQTT: 上报“车->云(dcu→MQTT→TCS→openapi)”命令请求

然后等待下发的command-v2s应答

--------------------------------------------------------------------------------------------------
- topic

/devices/{deviceid}/command-v2s

```
{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "ts":"20210304123456001",           //命令时间戳，“ts_command”构成唯一索引，即：每车不能在1ms内下发多条同一命令
        "command":"CHANGE_LANE",            //命令名，允许自定义命令名，必选字段,建议为 ARRIVED/WORK_START/WORK_END/WORK_CANCEL/CHANGE_LANE
        "params":"force=true, direction=LEFT", //命令参数，允许自定义命参数，可选字段。用于重要动作和工作流状态标识，如挂钩开始 挂钩完成
        "description":"向左换道请求",       //可读的命令描述，可选字段
        "max_duration":5                    //命令超时时长为5秒，5秒内没有收到MQTT/dcu应答dtc会重传，超过5秒dtc报超时，无此字段默认为5秒
    }
}

```

- MQTT应答

--------------------------------------------------------------------------------------------------

 MQTT->DCU: 下发“车->云v2s”命令应答

 网络应答自身上报的command_v2s命令

--------------------------------------------------------------------------------------------------
- topic

/devices/{deviceid}/command-v2s-resp

```
{
    "ts": "20231206114512000", 
    "deviceid": "zauto_01",
    "body": {
        "ts":"20210304123456001", //命令时间戳，“ts_command”构成唯一索引，即：每车不能在1ms内下发多条同一命令
        "command":"CHANGE_LANE",  //命令名，允许自定义命令名，必选字段
        "result": "",             //可选字段，自定义返回码，可在portal显示，或用于工作流的分支参数，
                                  

        //以下status选其一：

        //上报RUNNING表示命令没有执行完，后续还会回应答此消息
        "status": "RUNNING"

        //执行成功后应答SUCCEED
        "status": "SUCCEED"

        //失败，包括以下3种错误码
        "status": "FAILURE","error_msg": "001. parameter error"
        "status": "TIMEOUT","error_msg": "020. command timeouted"
        "status": "CANCELED","error_msg": "030. command canceled"
    }
}
```






