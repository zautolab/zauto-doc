lioslam_ros2 建图

 
1.录制
雷达正常后，录制
frame_id:laser_link

lioslam配置修改点云topic

    mapping = launch_ros.actions.Node(
        package='scanmatcher',
        executable='scanmatcher_node',
        parameters=[main_param_dir],
        remappings=[('/input_cloud','/points_raw')],
        output='screen'
        )
修改tf转换laser_link到base_link
          tf = launch_ros.actions.Node(
        package='tf2_ros',
        executable='static_transform_publisher',
        arguments=['0','0','0','0','0','0','1','base_link','laser_link']
        )



ros2 bag record -o lidarpointcloud.bag /pointcloud_raw
ros2 bag play xxx

2.直接建图
## 启动建图程序
- 启动雷达
source ~/autoware_ws/install/local_setup.bash
ros2 launch lslidar_driver lslidar_c32_slam.launch.py

- 启动建图
source ~/ros2_ws/install/local_setup.bash
ros2 launch lidarslam lidarslam_tukuba.launch.py

- global frame选择laser_link
- rviz2添加topic /points_raw

（若没有出现pose(绿色箭头)，重启雷达）

##移动车辆
出现pose后，移动车辆，超一个方向或环线移动（见截图-建图rviz.png）

等待lidarslam控制台输出submap 1 2 3 字样后可进行保存（见截图-建图控制台.png）

##保存pcd地图
ros2 service call /map_save std_srvs/Empty
默认生成map.pcd，保存在当前目录

##查看pcd
sudo apt install pcl-tools
pcl_viewer map.pcd

可以把map.pcd改名，防止再次建图覆盖掉

3.遥控程序和建图

遥控程序依赖autoware，所以需要单独启动遥控，再启动建图
只启动docker和车机驱动/遥控，不启动autoware（不启动雷达等设备）
按照非集成方式大致如下（摘自docker-ego启动.md）：

cd /home/dev/autoware

./docker/run.sh --devel

-宿主机cp
docker cp  /home/ubuntu/autoware_ws/autoware/param/ego_sensor_kit zauto_x:/autoware/install/individual_params/share/individual_params/config/default/ego_sensor_kit

docker cp  /home/ubuntu/autoware_ws/src/zauto_vehicle_interface/lib/libcontrolcan.so zauto_x:/usr/lib/libcontrolcan.so

-容器
source /autoware/install/local_setup.bash

source /home/ubuntu/autoware_ws/install/local_setup.bash

source /home/ubuntu/autoware_ws/src/mqtt_adapter/install/setup.bash

- 启动车机

ros2 launch zauto_vehicle_interface vehicle_ctrl_launch.xml

- 启动mqqt 适配器
pip install paho-mqtt
ros2 run mqtt_rclpy mqtt_ps
